-- kpmoran
-- THIS SCRIPT MAKES CALENDAR EVENTS FROM OMNIFOCUS TASKS WHOSE
-- DUE DATES AND ESTIMATED MINUTES FIELDS HAVE VALID VALUES.

property calendar_name : "OmniFocus" -- ENTER NAME OF YOUR CALENDAR

tell application "Calendar"
	set calendar_element to calendar calendar_name
end tell
tell application "OmniFocus"
	tell default document
		set theProjects to every flattened project
		repeat with project_ref in theProjects
			set task_elements to (every flattened task of project_ref whose �
				(completed is false) and (due date � missing value))
			repeat with item_ref in task_elements
				-- GET OMNIFOCUS TASKS
				set the_task to contents of item_ref
				set task_name to name of the_task
				set task_note to note of the_task
				set task_due to due date of the_task
				set proj_name to name of project_ref
				-- BUILD CALENDAR DATE
				set start_date to task_due
				set end_date to task_due
				-- CREATE CALENDAR EVENT
				tell application "Calendar"
					tell calendar_element
						if exists (first event whose (summary = proj_name & ": " & task_name)) then
							set currEvent to (first event whose summary = proj_name & ": " & task_name)
							set tempSum to {summary} of currEvent
							delete currEvent
							--tell application "System Events"
							--display dialog tempSum
							--end tell
							make new event with properties �
								{summary:proj_name & ": " & task_name, start date:start_date, end date:start_date} at calendar_element
							--delete (first event whose summary = task_name)
							--tell application "System Events"
							--display dialog "The task: " & task_name & " is already in your calendar"
							--end tell
						else
							make new event with properties �
								{summary:proj_name & ": " & task_name, start date:start_date, end date:start_date} at calendar_element
						end if
					end tell
				end tell
			end repeat
		end repeat
	end tell
end tell

tell application "OmniFocus"
	tell default document
		set theProjects to every flattened project
		repeat with project_ref in theProjects
			set task_elements to (every flattened task of project_ref whose �
				(completed is true) and (due date � missing value))
			repeat with item_ref in task_elements
				-- GET OMNIFOCUS TASKS
				set the_task to contents of item_ref
				set task_name to name of the_task
				set task_note to note of the_task
				set task_due to due date of the_task
				set proj_name to name of project_ref
				-- BUILD CALENDAR DATE
				set start_date to task_due
				set end_date to task_due
				-- CREATE CALENDAR EVENT
				tell application "Calendar"
					tell calendar_element
						if exists (first event whose (summary = proj_name & ": " & task_name)) then
							set currEvent to (first event whose summary = proj_name & ": " & task_name)
							set tempSum to {summary} of currEvent
							delete currEvent
							--tell application "System Events"
							--display dialog tempSum
							--end tell
							--tell application "System Events"
							--display dialog "The task: " & task_name & " is already in your calendar"
							--end tell
						end if
					end tell
				end tell
			end repeat
		end repeat
	end tell
end tell

tell application "Calendar" to activate
tell application "System Events"
	tell process "iCal"
		keystroke "r" using {command down}
	end tell
end tell
delay 10
tell application "Calendar"
	quit
end tell
tell application "System Events"
	display dialog "Omnifocus Tasks Synced to Calendar!"
end tell